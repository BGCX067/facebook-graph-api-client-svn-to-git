package com.fsl.facebook;

import java.io.UnsupportedEncodingException;

import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.util.URIUtil;

import com.fsl.facebook.model.UserProfile;

public class FaceBookGraphApiClient {

	public static final String GRAPH_API_URL = "https://graph.facebook.com/";
	private static final String SEP = "/";

	private String uid;
	private String accesstoken;

	private HttpClient client;

	public FaceBookGraphApiClient(String uid, String accesstoken) {
		this.uid = uid;
		this.accesstoken = accesstoken;

		// ( client = new HttpClient(new MultiThreadedHttpConnectionManager());
		client = new HttpClient();
		client.getHttpConnectionManager().getParams()
				.setConnectionTimeout(30000);
	}

	public UserProfile getUserInfo() {

		UserProfile user = null;

		StringBuffer url = new StringBuffer(GRAPH_API_URL);

		url.append(uid);

		try {

			NameValuePair pair = new NameValuePair("access_token",
					URIUtil.encodeQuery(accesstoken));

			NameValuePair[] pairs = new NameValuePair[] { pair };

			String response = callUrl(url.toString(), pairs);

			user = UserProfile.fromJson(response);

		} catch (URIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return user;
	}

	public void publishFeed(String message) {

		StringBuffer url = new StringBuffer(GRAPH_API_URL);

		url.append(uid).append(SEP).append("feed");

		try {
			
			NameValuePair access_token_pair = new NameValuePair("access_token",
					URIUtil.encodeQuery(accesstoken));
			NameValuePair pair = new NameValuePair("message",
					message);

			NameValuePair[] pairs = new NameValuePair[] {access_token_pair, pair };
			
			String response = sendPost(url.toString(), pairs);
			
			System.out.println(response);

		} catch (URIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String sendPost(String url, NameValuePair[] pairs) {

		String response = null;

		PostMethod post = new PostMethod(url);
		try {

			post.setQueryString(pairs);

			int iGetResultCode = client.executeMethod(post);

			if (iGetResultCode == 200) {
				final String strGetResponseBody = post
						.getResponseBodyAsString();

				if (strGetResponseBody != null) {
					response = strGetResponseBody;
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {

			post.releaseConnection();
		}

		return response;

	}

	private String callUrl(String url, NameValuePair[] pairs) {

		String response = null;
		GetMethod get = new GetMethod(url);
		try {

			get.setQueryString(pairs);
			get.setFollowRedirects(true);

			int iGetResultCode = client.executeMethod(get);

			if (iGetResultCode == 200) {
				final String strGetResponseBody = get.getResponseBodyAsString();

				if (strGetResponseBody != null) {
					response = strGetResponseBody;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {

			get.releaseConnection();
		}

		return response;
	}

}
