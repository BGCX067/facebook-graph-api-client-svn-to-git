package com.fsl.facebook.model;

import net.sf.json.JSONObject;

public class UserProfile {

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public HomeTown getHomeTown() {
		return homeTown;
	}

	public void setHomeTown(HomeTown homeTown) {
		this.homeTown = homeTown;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public int getTimezone() {
		return timezone;
	}

	public void setTimezone(int timezone) {
		this.timezone = timezone;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	protected String id;
	protected String name;
	protected String first_name;
	protected String last_name;
	protected String email;
	protected String birthday;

	protected Location location;
	protected HomeTown homeTown;

	protected String locale;
	protected int timezone;

	/*
	 * {"id":"667706320","name":"Joaquin Diez","first_name":"Joaquin","last_name"
	 * :"Diez",
	 * "link":"http:\/\/www.facebook.com\/joaquindiez","birthday":"06\/04\/1975",
	 * "hometown"
	 * :{"id":"112408052112026","name":"Segovia, Spain"},"location":{"id"
	 * :"106504859386230","name":"Madrid, Spain"},
	 * "email":"joaquindiez@gmail.com"
	 * ,"timezone":1,"locale":"en_US","verified":true
	 * ,"updated_time":"2010-12-13T14:56:18+0000"}
	 */

	public static UserProfile fromJson(String json) {

		UserProfile user = new UserProfile();

		JSONObject obj = JSONObject.fromObject(json);

		user.id = (String) obj.get("id");
		user.name = (String) obj.get("name");
		user.first_name = (String) obj.get("first_name");
		user.last_name = (String) obj.get("last_name");
		user.email = (String) obj.get("email");
		user.birthday = (String) obj.get("birthday");
		user.locale = (String) obj.get("locale");
		user.timezone = ((Integer) obj.get("timezone")).intValue();

		try {
			JSONObject jsonObject = obj.getJSONObject("location");
			user.location = Location.fromJsonObject(jsonObject);
		} catch (Throwable t) {
			t.printStackTrace();
		}
		try {
			JSONObject jsonObjectLocation = obj.getJSONObject("hometown");
			user.homeTown = HomeTown.fromJsonObject(jsonObjectLocation);

		} catch (Throwable t) {
			t.printStackTrace();	
		}
		return user;

	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getFirst_name() {
		return first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public String getEmail() {
		return email;
	}

	public String getBirthday() {
		return birthday;
	}
}
