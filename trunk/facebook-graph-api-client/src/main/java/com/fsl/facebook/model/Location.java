package com.fsl.facebook.model;

import net.sf.json.JSONObject;

public class Location {

	protected String id;
	protected String name;

	public static Location fromJsonObject(JSONObject jsonObject) {

		Location obj = null;

		if (jsonObject != null) {
			obj = new Location();

			obj = fromJsonObject(obj, jsonObject);
		}

		return obj;
	}

	public static Location fromJsonObject(Location o, JSONObject jsonObect) {

		o.id = (String) jsonObect.get("id");
		o.name = (String) jsonObect.get("name");

		return o;
	}

	public static Location fromJson(String json) {

		Location obj = new Location();

		JSONObject jsonObject = JSONObject.fromObject(json);

		obj = fromJsonObject(obj, jsonObject);

		return obj;

	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
