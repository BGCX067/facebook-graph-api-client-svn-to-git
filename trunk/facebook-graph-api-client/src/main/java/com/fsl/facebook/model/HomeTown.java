package com.fsl.facebook.model;

import net.sf.json.JSONObject;

public class HomeTown {

	protected String id;
	protected String name;

	public static HomeTown fromJsonObject(JSONObject jsonObject) {

		HomeTown obj = null;
		if (jsonObject != null) {
			 obj = new HomeTown();

			obj = fromJsonObject(obj, jsonObject);
		}

		return obj;
	}

	public static HomeTown fromJsonObject(HomeTown o, JSONObject jsonObject) {

		o.id = (String) jsonObject.get("id");
		o.name = (String) jsonObject.get("name");

		return o;
	}

	public static HomeTown fromJson(String json) {

		HomeTown obj = new HomeTown();

		JSONObject jsonObject = JSONObject.fromObject(json);

		obj = fromJsonObject(obj, jsonObject);

		return obj;

	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
